# Challenge

Install deps

    bunle install


# Running

Please run with

    bundle exec rail s

After this, the UI should accept files, and exhibit required behavior. I have
added a small report on my rationale, and how the algorithm works on the 'About'
page.

# Tests

There also exists tests

    bundle exec rake spec

# Thanks!

Was a pleasure meeting you.


# I'm just using one controller for everything, because there's no point making
# a static page controller for one page.
# @author psyomn
class WordsController < ApplicationController
  include CustomDataStructures

  def index
  end

  # The algorithmic part of the controller is factored out in lib/
  def submit
    file = params["words_list"]
    flash.clear
    begin
      @results = WordTree.new(file.read).process
    rescue WordTreeError
      flash[:word_tree_error] = "Unable to find a stacked word tree :("
    rescue BadFileError
      flash[:bad_file_error] = "You have uploaded an incompatible file!"
    rescue
      flash[:error] = "An unexpected error has occured. Contact site admin."
    end
    render layout: false
  end

  def about
    @ascii_to_vec = CodeRay.scan("
def string_to_vec(str)
  arr = Array.new(26, 0)
  str.chars.map { |e| arr[e.ord - 97] += 1 }
  arr
end", :ruby).div(line_numbers: :table)

    @ascii_inject = CodeRay.scan("
def matrix_successor?(m1, m2)
  ((0...26).inject(0) { |sum,el| (m1[el] - m2[el]).abs + sum }) == 1
end
", :ruby).div(line_numbers: :table)
  end
end

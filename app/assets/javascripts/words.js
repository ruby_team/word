$(document).ready(function(){
  $("#word-spinner").hide();
  $("#response-box").hide();
  $("#try-again").hide();

  $("#try-again").click(function(){
    $("#try-again").hide('slow');
    $("#response-box").hide('slow');
    $("#response-box").html('');
    $("#myform").show('slow');
  });
});

$(document).ajaxStart(function(){
  $("#word-spinner").show('slow');
  $("#myform").hide('slow');
}).ajaxComplete(function(event, xhr, options){
  $("#response-box").show('slow');
  $("#response-box").html(xhr.responseText);
}).ajaxStop(function(){
  $("#word-spinner").hide('slow');
  $("#try-again").show('slow');
});

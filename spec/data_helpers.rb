module DataHelpers
  def data_dir_path
    File.join(File.dirname(__FILE__), 'data')
  end

  def gen_file_path
    File.join(data_dir_path, 'wordlist_generalizations.dat')
  end

  def trad_file_path
    File.join(data_dir_path, 'wordlist_traditionless.dat')
  end

  def wordlist_file_path
    File.join(data_dir_path, 'wordlist.dat')
  end

  def empty_wordlist_file_path
    File.join(data_dir_path, 'wordlist_empty.dat')
  end

  def whitespace_file_path
    File.join(data_dir_path, 'wordlist_whitespace.dat')
  end

  def basic_file_path
    File.join(data_dir_path, 'wordlist_base.dat')
  end

  def foreign_word_path
    File.join(data_dir_path, 'wordlist_foreign.dat')
  end

  def binfile_path
    File.join(data_dir_path, 'binfile')
  end

  def wordlist_no_tree_path
    File.join(data_dir_path, 'wordlist_no_tree.dat')
  end
end

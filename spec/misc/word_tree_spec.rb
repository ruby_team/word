require 'spec_helper'
require 'data_helpers'
require 'custom_data_structures/word_tree'

include CustomDataStructures

describe CustomDataStructures do

  context WordTree do
    include DataHelpers
    include CustomDataStructures
    include CustomDataStructures::StringHelper

    it 'should raise an exception if there is no word stack tree' do
      data = File.read(wordlist_no_tree_path)
      expect { WordTree.new(data).process }.to raise_error(WordTreeError)
    end

    it 'should raise a bad file exception if the file is not text' do
      data = File.read(binfile_path)
      expect { WordTree.new(data) }.to raise_error(BadFileError)
    end

    it 'should raise BadFileError if empty file provided' do
      data = File.read(empty_wordlist_file_path)
      expect { WordTree.new(data) }.to raise_error(BadFileError)
    end

    it 'should raise BadFileError if whitespace file provided' do
      data = File.read(whitespace_file_path)
      expect { WordTree.new(data) }.to raise_error(BadFileError)
    end

    it 'should raise BadFileError if file is provided in wrong language' do
      data = File.read(foreign_word_path)
      expect { WordTree.new(data) }.to raise_error(BadFileError)
    end

    it 'should mark indices to delete' do
      indices = [0, 1, 2, 3, 5, 6, 7, 8]
      ret = WordTree.indices_to_delete(indices)
      expect(ret).to eq([5,6,7,8])
    end

    it 'should remove word lists which will never match' do
      data = File.read(gen_file_path)
      data_groups = data.split.sort.group_by{|e| e.size}
      WordTree.drop_if_skips!(data_groups)
      keys = data_groups.keys.sort
      expect(keys).to eq((2..25).to_a)
    end

    it 'should at least succeed on basic file' do
      data = File.read(basic_file_path)
      wt = WordTree.new(data).process
      ret = list_of_successors?(wt)
      expect(wt).to eq([
        'ire',
        'rite',
        'trite',
        'titres',
        'tinters',
        'tritones',
        'stationer',
        'iterations',
        'orientalist',
        'orientalists',
        'traditionless'
      ])
      expect(ret).to be true
    end

    it 'should detect generalizations & friends as a tree stack' do
      data = File.read(gen_file_path)
      wt   = WordTree.new(data).process
      ret  = list_of_successors?(wt)
      expect(ret).to be true
    end

    it 'should detect traditionless & friends as a tree stack' do
      data = File.read(trad_file_path)
      wt   = WordTree.new(data).process
      ret  = list_of_successors?(wt)
      expect(ret).to be true
    end

    def list_of_successors?(wt)
      ret  = true
      (0...wt.length-1).each do |x|
        v1 = string_to_vec(wt[x])
        v2 = string_to_vec(wt[x+1])
        ret &&= matrix_successor?(v1, v2)
      end
      ret
    end
  end

  context StringHelper do
    include StringHelper

    it 'should make proper word vectors' do
      arr = Array.new(26, 0)
      arr[0] = 1
      arr[1] = 1
      arr[2] = 1
      arr2 = arr.dup
      arr2[25] = 1

      vec = string_to_vec("abc")
      vec2 = string_to_vec("abcz")
      expect(vec).to eq(arr)
      expect(vec2).to eq(arr2)
    end

    it 'should identify vector successors' do
      word1 = 'stationer'
      word2 = 'tritones'
      v1 = string_to_vec(word1)
      v2 = string_to_vec(word2)
      expect(matrix_successor?(v1, v2)).to be true
      expect(matrix_successor?(v2, v1)).to be true
    end

    it 'should identify non successors' do
      word1 = 'potato'
      word2 = 'hahahaha'
      v1 = string_to_vec(word1)
      v2 = string_to_vec(word2)
      expect(matrix_successor?(v1, v2)).to be false
    end

    it 'should detect orientalist as successor of iterations' do
      word1 = 'orientalist'
      word2 = 'iterations'
      v1 = string_to_vec(word1)
      v2 = string_to_vec(word2)
      expect(matrix_successor?(v1, v2)).to be true
      expect(matrix_successor?(v2, v1)).to be true
    end

  end

end

module CustomDataStructures
  # You could add this as a refinement too, but I decided not to be very
  # adventurous for this exercise.
  module StringHelper
    # give string, get matrix/array representation
    def string_to_vec(str)
      arr = Array.new(26, 0)
      str.chars.map { |e| arr[e.ord - 97] += 1 }
      arr
    end

    # It's quicker to match numbers than splitting letters etc. Also an anagram
    # contains exactly the same number of letters, with the other word.
    # Therefore, the absolute difference of a sum of two anagrams, will be 1.
    def matrix_successor?(m1, m2)
      ((0...26).inject(0) { |sum,el| (m1[el] - m2[el]).abs + sum }) == 1
    end
  end

  # How to use:
  #   1) Provide the first word, which is one of the largest words
  #   2) use process(hash), and let it build the tree by itself
  #   3) when process ends, we return a bool: true if last processed word is of
  #      size 3. We use this bool in WordTree to figure out whether we need to
  #      search for the next large word onwards (fail), or if we have succeeded.
  #   4) If we have succeeded, we simply call traceback, and that will return us
  #      a list of words - the result we are seeking.
  class Tree
    include StringHelper
    # We begin at the root
    def initialize(term)
      @root = @navigator = Node.new(term, nil)
      @root.visited!
    end

    # Note: It should get a read only reference to the given hash
    def process(data_h)
      loop do
        @navigator.visited!

        if data_h.keys.size < 2
          # This means we've depleted possible combinations
          raise WordTreeError
        end

        if @navigator.children.empty?
          next_size = @navigator.term_size - 1
          children = data_h[next_size].select { |el| matrix_successor?(@navigator.matrix, el[1]) }
          @navigator.children += children.map { |el| Node.new(el, @navigator) }
        end

        if @navigator == @root && @root.all_children_visited?
          # Stopping case - we have exhausted all possible searches
          return false
        end

        if @navigator.children.empty?
          # hit a dead end. Does there exist any other nodes we have not visited
          # yet?
          rollback!
          next
        end

        next_child = @navigator.children.find { |el| !el.visited? }

        @navigator = next_child

        if next_child.term_size == 3
          # stopping case - we have found the last term
          return true
        end
      end
    end

    # Backtracking in case there's more than one successor. It will set the
    # navigator to the next available term. This emulates a 'dynamic' DFS.
    def rollback!
      while !@navigator.parent.nil? do
        # @navigator = @navigator.parent
        all_visited = @navigator.children.inject(true) { |sum,el| sum && el.visited }
        if all_visited
          @navigator = @navigator.parent
          next
        else
          # Set to unvisited term
          @navigator = @navigator.children.find { |el| !el.visited? }
          return
        end
      end
    end

    # make the navigator focus on the given term
    def set_current(term)
      if tmp = @navigator.children.find { |el| el.data == term }
        @navigator = tmp
        true
      else
        false
      end
    end

    # Call this when we know we have found the last word
    def traceback
      trace = @navigator # current pos
      ret = []
      while !trace.nil? do
        ret.push trace.term.dup
        trace = trace.parent
      end
      ret
    end

    def current_is_root?
      @navigator == @root
    end

    attr_accessor :root
    attr_accessor :navigator
  end

  class Node
    def initialize(term, parent)
      @data = term
      @visited = false
      @children = []
      @parent = parent
    end

    def visited?; @visited end
    def visited!; @visited = true end
    def term_size; @data[0].length end
    def term; @data[0] end
    def matrix; @data[1] end

    def all_children_visited?
      @children.inject(true) { |sum,el| el.visited && sum }
    end

    # @return Node on find, nil on no find
    def next_unvisited
      @children.find { |el| !el.visited? }
    end

    attr_accessor :parent
    attr_accessor :data
    attr_accessor :children
    # True when we try to find successor
    attr_accessor :visited
  end

  # The algorithm is isolated in this file. It separates the words in trees/nodes,
  # and then selects all the leaf nodes in order to form the words.
  #
  # In the context of the challenge, if the nth term is word, then the next term
  # n+1 is anagram(word_n, word_n+1) + letter. In other words, all the (same)
  # letters included in the next term + one new letter.
  #
  # This means that the next term is always one size bigger, therefore we can
  # group the words by size, and request per size.
  #
  # We can also perform an optimization by running the algorithm "upside down".
  # We start from the longest term, and work our way towards the smaller ones.
  #
  # @author psyomn
  class WordTree
    include StringHelper

    def initialize(data_s)
      validate_data(data_s)

      data_groups = data_s.split.sort.uniq.group_by{|e| e.size}

      data_groups.delete 1
      data_groups.delete 2
      WordTree.drop_if_skips!(data_groups)

      preprocess_data_groups!(data_groups)

      @results = []
    end

    # This is the actual algorithm
    def process
      # This takes the largest term, and attempts to match all the way down
      while !@data.empty? do
        curr_size = next_available_key
        curr_term = @data[curr_size].pop

        tree = Tree.new(curr_term)

        if tree.process(@data)
          # found
          @results = tree.traceback
          break
        end
      end

      result_meaning
    end

    # use for current largest word
    def next_available_key
      loop do
        k = @data.keys.max
        if k.nil?
          raise WordTreeError
        elsif @data[k].count == 0
          @data.delete k
          next
        else
          return k
        end
      end
    end

    attr_reader :data
    attr_reader :results

    private

    def preprocess_data_groups!(data_groups)
      @data = {}

      data_groups.each do |k,v|
        arr = []
        v.each do |word|
          arr.push [word, string_to_vec(word)]
        end
        @data[k] = arr
      end
    end

    # The logic we want to go through to evaluate what we exactly processed.
    def result_meaning
      if @results.empty?
        raise WordTreeError
      elsif @results.first.size != 3
        raise WordTreeError
      else
        @results
      end
    end

    # This does a bunch of checks against the provided data
    def validate_data(data_s)
      if data_s.length == 0
        # empty file
        raise BadFileError
      end

      if data_s.match(/^\s+$/)
        # blank whitespace file
        raise BadFileError
      end

      if !data_s.match(/^[[:alpha:]]+$/)
        # file contains non alphabetic characters
        raise BadFileError
      end

      if !data_s.ascii_only?
        # Reject anything else apart from ascii
        raise BadFileError
      end
    rescue ArgumentError
      # This happens if you provide a file which is not text (eg a bin)
      raise BadFileError
    end

    # There's no point in trying to run the algorithm if the difference is more
    # than one letter.
    def self.drop_if_skips!(data_group_h)
      keys = data_group_h.keys.sort
      indices_to_delete(keys).each do |el|
        data_group_h.delete el
      end
    end

    # if word sizes skip, then we cut at skip
    def self.indices_to_delete(ix_a)
      ix = 1
      while ix < ix_a.size do
        if (ix_a[ix - 1] - ix_a[ix]).abs == 1
          ix += 1
          next
        else
          return ix_a[ix...(ix_a.length)]
        end
      end
      []
    end
  end

  # Raise when something goes wrong in the WordTree
  class WordTreeError < StandardError; end

  # Raise when a bad file is given (empty, just whitespace)
  class BadFileError < StandardError; end

  # Raise when non alphabetic characters are given
  class IllegalTokenError < StandardError; end
end
